// Fill out your copyright notice in the Description page of Project Settings.


#include "LifePickUp.h"
#include "SnakeBase.h"

// Sets default values
ALifePickUp::ALifePickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALifePickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALifePickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALifePickUp::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->IncreaseLives();
			Snake->ShowMessage(FString(TEXT("You' ve got additional life!")), FColor::Emerald);
			OnEat();
		}
	}
}

