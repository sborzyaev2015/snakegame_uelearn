// Fill out your copyright notice in the Description page of Project Settings.



#include "PickUpFactory.h"


// Sets default values
APickUpFactory::APickUpFactory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MinCellX = -4;
	MaxCellX = 4;
	MinCellY = -4;
	MaxCellY = 4;
}

// Called when the game starts or when spawned
void APickUpFactory::BeginPlay()
{
	Super::BeginPlay();
	SpawnNewPickUp();
}

// Called every frame
void APickUpFactory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APickUpFactory::SpawnNewPickUp()
{
	std::pair<float, float> Coords = GetCoordinates();
	EPickUpType PickUpType = GetPickUpType();

	// ������ ����������� ��������
	FVector PickUpLocation(Coords.first, Coords.second, 30);

	// ������� ������������� ��� ��������
	FTransform PickUpTransform = FTransform(PickUpLocation);

	
	switch (PickUpType)
	{
	case EPickUpType::FOOD:
		CreatePickUp(PickUpFoodClass, PickUpTransform);
		break;
	case EPickUpType::SPEED:
		CreatePickUp(PickUpSpeedClass, PickUpTransform);
		break;
	case EPickUpType::LIFE:
		CreatePickUp(PickUpLifeClass, PickUpTransform);
		break;
	}
}

std::pair<float, float> APickUpFactory::GetCoordinates()
{
	int x_factor = GetRandom<int>(MinCellX, MaxCellX);
	int y_factor = GetRandom<int>(MinCellY, MaxCellY);

	return std::make_pair(x_factor*100.f, y_factor*100.f);
}

EPickUpType APickUpFactory::GetPickUpType()
{
	int mean = GetRandom<int>(1, 10);
	if (mean < 8)
	{
		return EPickUpType::FOOD;
	}
	else if (mean == 8 || mean == 9)
	{
		return EPickUpType::SPEED;
	}

	return EPickUpType::LIFE;
}

void APickUpFactory::CreatePickUp(TSubclassOf<APickUpBase> PickUpClass, FTransform PickUpTransform)
{
	APickUpBase* NewPickUp = GetWorld()->SpawnActor<APickUpBase>(PickUpClass, PickUpTransform);
	if(IsValid(NewPickUp))
	{
		NewPickUp->ItemFactory = this;
	}
	else
	{
		// �������� ������� ������ ������, �� ������ ���� ��� ������ ������� �� �������� �� ����� ������.
		SpawnNewPickUp();
	}
		
	
}



