// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedUpPickUp.h"
#include "SnakeBase.h"

// Sets default values
ASpeedUpPickUp::ASpeedUpPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedUpPickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedUpPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedUpPickUp::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetSpeed(Snake->MovementSpeed - 0.1f);

			if (Snake->MovementSpeed < 0.2)
				Snake->ShowMessage(FString(TEXT("Max Speed!")), FColor::Blue);
			else
				Snake->ShowMessage(FString(TEXT("Speed Up!")), FColor::Blue);

			OnEat();
		}
	}
}

