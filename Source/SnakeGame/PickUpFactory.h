// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUpBase.h"
#include <random>
#include "PickUpFactory.generated.h"

UENUM()
enum class EPickUpType {
	FOOD,
	LIFE,
	SPEED,
};

UCLASS()
class SNAKEGAME_API APickUpFactory : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUpFactory();

	// �������� ������������ ������� ����
	UPROPERTY(EditDefaultsOnly)
	int32 MinCellX;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxCellX;

	UPROPERTY(EditDefaultsOnly)
	int32 MinCellY;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxCellY;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APickUpBase> PickUpFoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APickUpBase> PickUpSpeedClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APickUpBase> PickUpLifeClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	void SpawnNewPickUp();

	std::pair<float, float> GetCoordinates();

	EPickUpType GetPickUpType();

	template<typename T>
	int GetRandom(T min_value, T max_value)
	{
		std::random_device r;
		std::default_random_engine e1(r());
		std::uniform_int_distribution<T> uniform_dist(min_value, max_value);
		return uniform_dist(e1);
	}


private:
	void CreatePickUp(TSubclassOf<APickUpBase> PickUpClass, FTransform Transform);
};
