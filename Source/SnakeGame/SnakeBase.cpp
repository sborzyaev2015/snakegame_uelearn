// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	MovementSpeed = 0.5;
	MaxSpeed = 0.1;
	LivesCount = 3;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetSpeed(MovementSpeed);
	SpawnNewSnake();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)

{   
	for (int i = 0; i < ElementsNum; ++i)
	{
		// ������ ����������� ��������
		FVector NewLocation(FVector(SnakeElements.Num() * ElementSize, 0, 50));

		// ������� ������������� ��� ��������
		FTransform NewTransform = FTransform(NewLocation);

		// ����� ������ �������� ������. ���������� ����� ������ ���� ����������� ASnakeElementBase � ����� ���������� ����� Blueprints
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);


		NewSnakeElem->SnakeOwner = this;
		NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirst();
		}
	}
}

void ASnakeBase::Move()
{

	FVector MovementVector(ForceInitToZero);
	switch (LastMovement)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	default:
		break;
	}


	SnakeElements[0]->ToggleCollision();
	
	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentEl = SnakeElements[i];
		auto PrevEl = SnakeElements[i-1];
		FVector PrevLocation = PrevEl->GetActorLocation();
		CurrentEl->SetActorLocation(PrevLocation);

		SetVisible(CurrentEl);
	}
	// ��������� ������
	SetVisible(SnakeElements[0]);
	// ����������� ������
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SpawnNewSnake()
{
	if (SnakeElements.Num() > 0)
	{
		for (ASnakeElementBase* SnakeElement : SnakeElements)
		{
			SnakeElement->Destroy();
		}
		SnakeElements.Empty();
	}

	LastMovement = EMovementDirection::DOWN;
	SetSpeed(0.5f);
	AddSnakeElement(4);
}

void ASnakeBase::SetVisible(ASnakeElementBase* SnakeElement)
{
	// ��������� ��������� ��������
	if(IsValid(SnakeElement))
	{
		if (SnakeElement->IsHidden())
		{
			SnakeElement->SetHidden(false);
		}
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::IncreaseLives()
{
	LivesCount++;
}

void ASnakeBase::DecreaseLives()
{
	LivesCount--;

	FString Msg = TEXT("Lives left: ");
	Msg.AppendInt(LivesCount);
	ShowMessage(Msg);

	if (LivesCount > 0)
	{
		SpawnNewSnake();
		return;
	}
	Destroy();
}

void ASnakeBase::SetSpeed(float speed)
{
	if (speed > MaxSpeed)
	{
		MovementSpeed = speed;
		SetActorTickInterval(MovementSpeed);
	}
}

void ASnakeBase::ShowMessage(FString Msg, FColor Color)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, Color, Msg);
}
